const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const primeracollectionShema = new Schema({
    word:{
        type: String,
        trim: true,
        
    },
    allowed:{
        type: Boolean,
        
    },
    status:{
        type: String,
        trim: true,
        
    },
    dateCreated:{
        type: Date,
        
    },
    lastDateUpdate:{
        type: Date,
    },
    dateDeleted:{
        type: Date,
    },



});

module.exports = mongoose.model('PrimeraCollection', primeracollectionShema);