//configuracion de rutas para nuestra API
const express = require('express');

const router = express.Router();

//importar controlador de PrimeraCollection
const PrimeraCollectionController = require('../controllers/PrimeraCollectionController');

//definir rutas
module.exports = function(){
    // get: /PrimeraCollection
    router.get('/PrimeraCollection', PrimeraCollectionController.list);
    return router;
}