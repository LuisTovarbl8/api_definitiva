//importaciones
const express =require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes');

const app = express();
//es como conectamos nuestra api a mongodb

mongoose.Promise = global.Promise;
mongoose.connect(
    'mongodb://localhost/API_DEFINITIVA',
    {
        useNewUrlParser: true,
    }
);

//habilitar body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

//habilitar cors
app.use(cors());

//peticio de tipo get en nuestro servidor local
app.use('/', routes());
//que nuestra aplicacion escuche en el puerto 5000
app.listen(5000, function(){
    console.log('Servidor web Express en ejecucion')
});

