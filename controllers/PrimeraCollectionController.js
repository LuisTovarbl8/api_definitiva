const PrimeraCollection = require('../models/PrimeraCollection');

//primera accion: list

exports.list = async(req,res)=>{
    try{
         //esto nos devolver todos los datos de la collectio
        const primeracollection =await PrimeraCollection.find({});
        res.json(primeracollection);
    }catch(error){
        console.log(error);
        res.send(error);
        next();
    }
   
};